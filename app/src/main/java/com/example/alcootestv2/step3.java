package com.example.alcootestv2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class step3 extends AppCompatActivity {
    RequestQueue requestQueue;
    String showUrl = "https://www.foriours.fr/TP-SIO/alcootestv2/showStudents.php";
    String Item ;
    String Item1 ;
    double total;
    public static int ari = 0;
    public static String[] Items ;
    public static String[] Itemsa ;
    public static String[] Itemsb ;


    Button next,prev;
    SearchView sv;
    ListView mobile_list,lv_aj;
    // TextView result;
    ArrayList<String> mobileArray= new ArrayList<String>();


    ArrayList<String> ar= new ArrayList<String>();
    ArrayList<Float> degar= new ArrayList<Float>();



    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step3);
        sv = (SearchView) findViewById(R.id.sv);
        mobile_list = (ListView) findViewById(R.id.lv_rec);
        lv_aj = findViewById(R.id.lv_aj);

        final ArrayAdapter adapter = new ArrayAdapter<String>(this,
                R.layout.activity_listview, mobileArray);

        final ArrayAdapter adapteraj = new ArrayAdapter<String>(this,
                R.layout.activity_listview1, ar);


        next = (Button) findViewById(R.id.next2);
        prev = (Button) findViewById(R.id.prev2);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               String ari = ar.toString();
                startActivity(new Intent(step3.this, step4.class));
            }
        });
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(step3.this, step2.class));
                total = 0;
            }
        });




        requestQueue = Volley.newRequestQueue(getApplicationContext());

       /* btsync.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                System.out.println("ww");*/
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                        showUrl, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println(response.toString()+"wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww");
                        try {
                            JSONArray mot = response.getJSONArray("alcool1");
                            for (int i = 0; i < mot.length(); i++) {
                                JSONObject moti = mot.getJSONObject(i);

                                String marque = moti.getString("Marque");
                                String ml = moti.getString("ml");
                                String deg = moti.getString("deg");


                                mobileArray.add("Marque : " + marque + " \n"  + "Quantité : " + ml + " \n" + "Taux alcoolimie : " + deg);
                                // result.append(nom + " " + descr + " " + " \n");

                            }

                            //result.append("===\n");
                            ListView listView = (ListView) findViewById(R.id.lv_rec);
                            listView.setAdapter(adapter);

                            //SearchView searchView = (SearchView) sv.getActionView();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                            @Override
                            public boolean onQueryTextSubmit(String text) {
                                // TODO Auto-generated method stub
                                return false;
                            }

                            @Override
                            public boolean onQueryTextChange(String text) {

                                adapter.getFilter().filter(text);

                                return false;
                            }
                        });



                        mobile_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                                Item = (String) parent.getItemAtPosition(position);
                                Items = Item.split(":");
                                Itemsa = Items[1].split(" ");
                                Itemsb = Items[2].split(" ");
                                ar.add("Marque : " + Itemsa[1] + " \n"  + "Alcool en (%) : " + Items[3] + " \n" + "Quantité en ml : " + Itemsb[1] );
                                    float deg = (Float.valueOf(Items[3]));
                                    float ml = (Float.valueOf(Itemsb[1]));
                                    double dens = 0.8;
                                    step2 st = new step2();
                                    double diff = st.diffusion;
                                    System.out.println(diff);
                                    double ma = st.m;
                                    System.out.println(ma);
                                    total = total + (1*ml*deg*dens)/(diff*ma);
                                    System.out.println(total);

                                ListView listView1 = (ListView) findViewById(R.id.lv_aj);
                                listView1.setAdapter(adapteraj);


                            }




                        });
                        lv_aj.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> test, View v, int i, long id1) {
                                Item1 =  (String) test.getItemAtPosition(i);
                                System.out.println(Item1+"wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww2");
                                ar.remove(Item1);
                                ListView listView1 = (ListView) findViewById(R.id.lv_aj);
                                listView1.setAdapter(adapteraj);


                            }

                        });




                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.append(error.getMessage());

                    }
                });
                requestQueue.add(jsonObjectRequest);
            }
 /*       });




    }*/
 public ArrayList<String> getAr() {
     return ar;
 }

}

